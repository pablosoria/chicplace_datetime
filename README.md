### Personal Time class example ###

* Supports 12h and 24h format
* Tested

### Installation (if you want run tests) ###


```
#!bash

composer install
```

### Use ###
```
#!bash

php Task/addTimeTask.php
```
* Enter time like 1:10pm / 01:10pm / 13:10
* Enter minutes