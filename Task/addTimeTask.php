#!/usr/bin/php
<?php

namespace Pablo\Task;

require dirname(__FILE__).'/../vendor/autoload.php';

use Exception;
use Pablo\Time\Time;

echo "Enter one hour in 12h or 24h format\n";
$time  =  trim( fgets( STDIN ) );
try {
    $result = new Time($time);
} catch (Exception $e) {
    echo $e->getMessage()."\n";
    die();
}

echo "Enter minutes you want to add\n";
$minutes  =  trim( fgets( STDIN ) );
try {
    $result->addMinutes($minutes);
} catch (Exception $e) {
    echo $e->getMessage()."\n";
    die();
}

echo "Result : ". $result->format(12).'   '. $result->format(24)."\n";
