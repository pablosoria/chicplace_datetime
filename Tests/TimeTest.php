<?php

namespace Pablo\Test;

require '../vendor/autoload.php';
use Exception;
use Pablo\Tests\Infrastructure\PHPUnit_Pablo;
use Pablo\Time\Time;

class TimeTest extends PHPUnit_Pablo
{
    public function testValidateFormat12()
    {
        $time = new Time('2:00am');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00 am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00AM'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00 AM'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00pm'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00PM'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00 pm'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00 PM'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('12:59am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('13:00am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format', $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('0:00pm'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('10:00  pm'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('3:60am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('003:20am'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('3:15sm'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');
    }

    public function testValidateFormat24()
    {
        $time = new Time('22:00');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('01:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('02:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('14:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('1:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('2:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('15:01'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('00:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('0:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals(true, $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('24:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format', $tmp, 'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('16:60'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('0:0'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('13:1'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

        try {
            $tmp = $this->invokeMethod($time, 'validateFormat', array('101:00'));
        } catch (Exception $e) {
            $tmp = $e->getMessage();
        }
        $this->assertEquals('Invalid time format',$tmp,'Format validation failed!');

    }

    public function testConvert12To24h()
    {
        $time = new Time('2:00pm');
        $this->assertEquals('14:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('02:00pm');
        $this->assertEquals('14:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('12:00am');
        $this->assertEquals('00:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('12:00pm');
        $this->assertEquals('12:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('09:00am');
        $this->assertEquals('09:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('8:00 am');
        $this->assertEquals('08:00', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('11:59 pm');
        $this->assertEquals('23:59', $time->format(24), 'Failed converting format 12h to 24h');

        $time = new Time('11:59 am');
        $this->assertEquals('11:59', $time->format(24), 'Failed converting format 12h to 24h');
    }

    public function testConvertTimeToMinutes()
    {
        $time = new Time('2:00pm');
        $this->assertEquals(14*60, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('2:00 pm');
        $this->assertEquals(14*60, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('2:59 pm');
        $this->assertEquals(14*60+59, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('11:59pm');
        $this->assertEquals(23*60+59, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('12:00 pm');
        $this->assertEquals(12*60, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('12:00am');
        $this->assertEquals(0, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('23:15');
        $this->assertEquals(23*60+15, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

        $time = new Time('0:01');
        $this->assertEquals(1, $time->convertTimeToMinutes(), 'Failed converting Time to minutes');

    }

    public function testConvertMinutesTo24h()
    {
        $time = new Time('10:00');

        $this->assertEquals('00:30', $time->convertMinutesTo24h(30), 'Failed converting minutes to Time 24h');
        $this->assertEquals('00:00', $time->convertMinutesTo24h(0), 'Failed converting minutes to Time 24h');
        $this->assertEquals('01:00', $time->convertMinutesTo24h(60), 'Failed converting minutes to Time 24h');
        $this->assertEquals('12:00', $time->convertMinutesTo24h(720), 'Failed converting minutes to Time 24h');
        $this->assertEquals('12:01', $time->convertMinutesTo24h(721), 'Failed converting minutes to Time 24h');
        $this->assertEquals('23:59', $time->convertMinutesTo24h(1439), 'Failed converting minutes to Time 24h');
        $this->assertEquals('08:46', $time->convertMinutesTo24h(526), 'Failed converting minutes to Time 24h');

    }

    public function testAddMinutes()
    {
        $time = new Time('00:00');
        $this->assertEquals('00:30', $time->addMinutes(30)->format(24), 'Failed adding minutes');

        $time = new Time('00:00');
        $this->assertEquals('23:59', $time->addMinutes(1439)->format(24), 'Failed adding minutes');

        $time = new Time('00:00');
        $this->assertEquals('00:00', $time->addMinutes(1440)->format(24), 'Failed adding minutes');

        $time = new Time('00:00');
        $this->assertEquals('00:01', $time->addMinutes(1441)->format(24), 'Failed adding minutes');

        $time = new Time('00:00');
        $this->assertEquals('00:15', $time->addMinutes(4335)->format(24), 'Failed adding minutes');

        $time = new Time('05:45');
        $this->assertEquals('05:47', $time->addMinutes(2)->format(24), 'Failed adding minutes');

        $time = new Time('05:45');
        $this->assertEquals('06:45', $time->addMinutes(60)->format(24), 'Failed adding minutes');

        $time = new Time('12:00');
        $this->assertEquals('12:00', $time->addMinutes(0)->format(24), 'Failed adding minutes');

        $time = new Time('12:00');
        $this->assertEquals('12:01', $time->addMinutes(1)->format(24), 'Failed adding minutes');

        $time = new Time('16:59');
        $this->assertEquals('17:00', $time->addMinutes(1)->format(24), 'Failed adding minutes');

        $time = new Time('11:59pm');
        $this->assertEquals('00:59', $time->addMinutes(60)->format(24), 'Failed adding minutes');
    }

    public function testFormat24hTo12()
    {
        $time = new Time('0:00');
        $this->assertEquals('12:00am', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('00:00');
        $this->assertEquals('12:00am', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('00:11');
        $this->assertEquals('12:11am', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('01:45');
        $this->assertEquals('01:45am', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('1:00');
        $this->assertEquals('01:00am', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('12:00');
        $this->assertEquals('12:00pm', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('14:10');
        $this->assertEquals('02:10pm', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('15:01');
        $this->assertEquals('03:01pm', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('23:59');
        $this->assertEquals('11:59pm', $time->format(12), 'Failed converting Time format');
        $this->assertEquals(12, $time->getFormat(), 'Failed property format converting Time format');

    }

    public function testFormat12hTo24()
    {
        $time = new Time('12:00am');
        $this->assertEquals('00:00', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('12:00 am');
        $this->assertEquals('00:00', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('12:59am');
        $this->assertEquals('00:59', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('1:05am');
        $this->assertEquals('01:05', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('01:09am');
        $this->assertEquals('01:09', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('11:45am');
        $this->assertEquals('11:45', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('12:00pm');
        $this->assertEquals('12:00', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('12:20 pm');
        $this->assertEquals('12:20', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('10:35pm');
        $this->assertEquals('22:35', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

        $time = new Time('11:59pm');
        $this->assertEquals('23:59', $time->format(24), 'Failed converting Time format');
        $this->assertEquals(24, $time->getFormat(), 'Failed property format converting Time format');

    }

    public function testIsNaturalNumber()
    {
        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('-1.0'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('-2'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('-0.1'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('1.2'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('2.0'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('3.456'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('abc'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(-1.0));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(-2));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(-0.1));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(1.2));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(3.456));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(2.0));
        $this->assertEquals(true, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(30));
        $this->assertEquals(true, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(456));
        $this->assertEquals(true, $tmp, 'Checking natural number failed!');

        // Special cases
        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('0x1A'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array('0123'));
        $this->assertEquals(false, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(0x1A));
        $this->assertEquals(true, $tmp, 'Checking natural number failed!');

        $time = new Time('13:00');
        $tmp = $this->invokeMethod($time, 'isNaturalNumber', array(0123));
        $this->assertEquals(true, $tmp, 'Checking natural number failed!');

    }
}
