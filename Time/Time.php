<?php

namespace Pablo\Time;

use Exception;

class Time {

    private $time;
    private $format;

    public function __construct($time)
    {
        $this->validateFormat($time);
        $this->time = $time;
    }

    public function getFormat()
    {
        return $this->format;
    }

    private function convert12To24h ()
    {
        $tmp = explode(':',$this->time);
        $hours = $tmp[0];
        $minutes = substr($tmp[1],0,2);
        $nightOrDay = trim(substr($tmp[1],2));

        if ($nightOrDay !== 'am' && $nightOrDay !== 'pm' ) {
            throw new Exception('Only accepted am or pm suffixes');
        }

        if ($nightOrDay == 'am' && $hours == 12) {
            $hour24 = 00;
        }elseif ($nightOrDay == 'pm' && $hours != 12) {
            $hour24 = $hours+12;
        } else {
            $hour24 = $hours;
        }

        $this->time = str_pad($hour24,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT);
        $this->format = 24;

        return $this;
    }

    public function convertTimeToMinutes()
    {
        if ($this->format == 12) {
            $this->convert12To24h();
        }
        $tmp = explode(':',$this->time);
        $hours = $tmp[0];
        $minutes = $tmp[1];

        $total = $hours*60 + $minutes;
        return $total;
    }

    public function convertMinutesTo24h($totalMinutes)
    {
        $days = explode('.',$totalMinutes / (24 * 60));
        $days = $days[0];
        $minutesReduced = $totalMinutes % (24 * 60);

        $hours = explode('.',$minutesReduced / 60);
        $hours = $hours[0];
        $minutes = $minutesReduced % 60;

        $time24h = str_pad($hours,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT);
        return $time24h;
    }

    public function addMinutes($minutes)
    {
        if (!$this->isNaturalNumber($minutes)) {
            throw new Exception('Only natural numbers allowed');
        }

        $newTimeMinutes = $this->convertTimeToMinutes() + $minutes;
        $this->time = $this->convertMinutesTo24h($newTimeMinutes);
        return $this;
    }

    private function convert24hTo12()
    {
        $tmp = explode(':',$this->time);
        $hours = $tmp[0];
        $minutes = $tmp[1];

        if ($hours == 00) {
            $hour12 = 12;
            $nightOrDay = 'am';
        }elseif ($hours >= 1 && $hours <= 11) {
            $nightOrDay = 'am';
            $hour12 = $hours;
        }elseif ($hours == 12) {
            $nightOrDay = 'pm';
            $hour12 = $hours;
        }elseif ($hours >= 13 && $hours <= 23) {
            $hour12 = $hours - 12;
            $nightOrDay = 'pm';
        }

        $time12h =  str_pad($hour12,2,'0',STR_PAD_LEFT).':'.str_pad($minutes,2,'0',STR_PAD_LEFT).$nightOrDay;
        $this->time = $time12h;
        $this->format = 12;

        return $this;
    }

    private function validateFormat($time)
    {
        $pattern12 = '/^(1[012]|0?[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)$/';
        $pattern24 = '/^([01]?[0-9]|2[0-3]):[0-5][0-9]$/';

        $condition12 = preg_match($pattern12,$time);
        $condition24 = preg_match($pattern24,$time);

        if ($condition12 === 1) {
            $this->format = 12;
        } elseif ($condition24 === 1) {
            $this->format = 24;
        }elseif ($condition12 === 0 || $condition24 === 0) {
            throw new Exception('Invalid time format');
        } elseif ($condition12 === false || $condition24 === false) {
            throw new Exception('Error evaluating regex expression');
        }

        return true;
    }

    public function format($format)
    {
        $this->isValidFormat($format);
        if ($format == $this->format) {
            true;
        } elseif($format == 12) {
            $this->convert24hTo12();
        } else {
            $this->convert12To24h();
        }
        return $this->time;
    }

    private function isValidFormat($format)
    {
        if ($format == 24 || $format == 12) {
            return;
        } else {
            throw new Exception('Invalid Time format. Valid formats: 12 and 24.');
        }
    }

    /**
     * @param $num
     *
     * @return bool
     * @throws Exception
     */
    private function isNaturalNumber($num)
    {
        $options = array(
            'options' => array('min_range' => 0)
        );

        if (filter_var($num, FILTER_VALIDATE_INT, $options) === false) {
            return false;
        }
        return true;
    }

}
